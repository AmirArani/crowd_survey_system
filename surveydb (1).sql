-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 02, 2020 at 08:47 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `surveydb`
--

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `n_id` varchar(50) NOT NULL,
  `subject` varchar(50) NOT NULL,
  `body` varchar(500) NOT NULL,
  `date` date NOT NULL,
  `pic` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `option`
--

CREATE TABLE `option` (
  `v_id` varchar(50) NOT NULL,
  `option` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `option`
--

INSERT INTO `option` (`v_id`, `option`) VALUES
('5edf604c3de3d', 'good'),
('5edf604c3de3d', 'bad'),
('5edf777d92a9b', 'yes'),
('5edf777d92a9b', 'no'),
('5ee4eccc22357', 'yes'),
('5ee4eccc22357', 'no'),
('5ee4ed98093b8', 'yes'),
('5ee4ed98093b8', 'no'),
('5ee4edd4d5bf4', 'yes'),
('5ee4edd4d5bf4', 'no'),
('5ee4ee2d469b0', 'yes'),
('5ee4ee2d469b0', 'no'),
('5ee4ef5f23d37', 'yes'),
('5ee4ef5f23d37', 'no'),
('5ee4ef911cd49', 'yes'),
('5ee4ef911cd49', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `u_id` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `code` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `region` varchar(50) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`u_id`, `name`, `lname`, `username`, `password`, `code`, `phone`, `email`, `region`, `status`) VALUES
('1', 'ali', 'hossieni', 'ali_h_se', '12345678', '125', '0913', 'ali@gmail.com', 'kashan', 'Active'),
('12', 'ali', 'ho', 'alih_se', '12345678', '125', '0913', 'gmail', 'kashan', 'Block'),
('5ede6de7af000', 'ali', '12345678', 'ali', '12345678', '125', '0913', 'ali@gmail.com', 'kashan', 'Block'),
('5edf6b2279d7f', 'ali', '12345678', 'ali_h_s', '12345678', '125', '0913', 'ali@gmail.com', 'kashan', 'Active'),
('5edf6b94e9d24', 'ali', '12345678', 'ali_h_sw', '12345678', '1250', '0913', 'ali@gmail.com', 'kashan', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `vote`
--

CREATE TABLE `vote` (
  `v_id` varchar(50) NOT NULL,
  `u_id` varchar(50) NOT NULL,
  `subject` varchar(50) NOT NULL,
  `pic` longtext NOT NULL,
  `generic` varchar(50) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vote`
--

INSERT INTO `vote` (`v_id`, `u_id`, `subject`, `pic`, `generic`, `date`) VALUES
('5edf777d92a9b', '1', 'what', 'aaaaaaaaa', '1', '2020-06-09'),
('5ee4ef911cd49', '1', 'what', 'image/5ee4ef911cd49.png', '1', '2020-06-13'),
('66', '1', 'done?', 'ssddd', '1', '2020-05-29'),
('77', '1', 'how?', 'asdad', '0', '2020-05-01');

-- --------------------------------------------------------

--
-- Table structure for table `voting`
--

CREATE TABLE `voting` (
  `v_id` varchar(50) NOT NULL,
  `u_id` varchar(50) NOT NULL,
  `option` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `voting`
--

INSERT INTO `voting` (`v_id`, `u_id`, `option`) VALUES
('5edf604c3de3d', '1', 'good'),
('5edf604c3de3d', '12', 'bad'),
('5edf777d92a9b', '1', 'yes'),
('5edf604c3de3d', '12', 'good');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`n_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`u_id`);

--
-- Indexes for table `vote`
--
ALTER TABLE `vote`
  ADD PRIMARY KEY (`v_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
