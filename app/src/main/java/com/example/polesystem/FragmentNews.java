package com.example.polesystem;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.loopj.android.http.HttpGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

public class FragmentNews extends Fragment {

    private static final String TAG = "npq: News Fragment";

    Context context;
    RecyclerView recent;
    RecyclerView mostSeen;
    RecyclerView shahrdari;
    RecyclerView kashan;
    NetworkImageView topNewsImage;
    TextView topNewsText;
    List<News> recentNewsList;
    List<News> mostSeenNewsList;
    List<News> shahrdariNewsList;
    List<News> kashanNewsList;
    NewsAdapter recent_adapter;
    NewsAdapter mostSeen_adapter;
    NewsAdapter kashan_adapter;
    NewsAdapter shahrdari_adapter;

    Dialog news_dialog;
    NetworkImageView newsDialogImage;
    TextView tv_newsDialog_title;
    TextView tv_newsDialog_body;
    TextView tv_newsDialog_seenCount;
    TextView tv_newsDialog_date;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview=inflater.inflate(R.layout.newsslide,container,false);
        return rootview;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getContext();
        topNewsImage = view.findViewById(R.id.imageslide);
        topNewsText = view.findViewById(R.id.sidetext);

        recent = view.findViewById(R.id.newslist1);
        mostSeen = view.findViewById(R.id.newslist2);
        shahrdari = view.findViewById(R.id.newslist3);
        kashan = view.findViewById(R.id.newslist4);

        recentNewsList = new ArrayList<>();
        mostSeenNewsList = new ArrayList<>();
        shahrdariNewsList = new ArrayList<>();
        kashanNewsList = new ArrayList<>();

        //TODO: GET ALL NEWS
        getAllNewsFromServer();

        //TODO: _DONE_ CHANGE it and get one last news. but remove this latest news from recent news recycler view
        topNewsImage.setImageResource(R.drawable.asfalt);
        topNewsText.setText("در حال دریافت اطلاعات!");

        recent_adapter = new NewsAdapter(recentNewsList,context);
        recent.setAdapter(recent_adapter);
        recent.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false));
        recent.addOnItemTouchListener(new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                getNewsInformationANDShowDialog(recentNewsList.get(position));
            }
        }));


        mostSeen_adapter = new NewsAdapter(mostSeenNewsList,context);
        mostSeen.setAdapter(mostSeen_adapter);
        mostSeen.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false));
        mostSeen.addOnItemTouchListener(new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                getNewsInformationANDShowDialog(mostSeenNewsList.get(position));
            }
        }));

        shahrdari_adapter = new NewsAdapter(shahrdariNewsList,context);
        shahrdari.setAdapter(shahrdari_adapter);
        shahrdari.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false));
        shahrdari.addOnItemTouchListener(new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                getNewsInformationANDShowDialog(shahrdariNewsList.get(position));
            }
        }));

        kashan_adapter = new NewsAdapter(kashanNewsList,context);
        kashan.setAdapter(kashan_adapter);
        kashan.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false));
        kashan.addOnItemTouchListener(new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                getNewsInformationANDShowDialog(kashanNewsList.get(position));
            }
        }));
    }

    private void getNewsInformationANDShowDialog(final News news) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String data = null;
                    data = URLEncoder.encode("n_id", "UTF-8") + "=" +
                            URLEncoder.encode(news.getN_id(), "UTF-8");

                    URL url = new URL(serverConstants.increaseNewsSeenCount_URL);
                    URLConnection conn = url.openConnection();
                    conn.setDoOutput(true);
                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                    wr.write( data );
                    wr.flush();
                    BufferedReader reader = new BufferedReader(new
                            InputStreamReader(conn.getInputStream()));
                    final StringBuilder sb = new StringBuilder();
                    String line;
                    // Read Server Response
                    while((line = reader.readLine()) != null) { sb.append(line); }

                    Log.i(TAG, "callAPI: "+sb.toString());

                    if (sb.toString().equals("{\"response\":\"added\"}")){
                        Log.i(TAG, "getNewsInformation: SeenCount Increased!");
                    }else if (sb.toString().equals("{\"response\":\"dont added\"}")){
                        Log.i(TAG, "getNewsInformation: SeenCount NOT Increased!");
                    }
                } catch (UnsupportedEncodingException e) {
                    Log.i(TAG, "callAPI: error: UnsupportedEncodingException");
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    Log.i(TAG, "callAPI: error: MalformedURLException");
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.i(TAG, "callAPI: error: IOException");
                    e.printStackTrace();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        news_dialog = new Dialog(context);
                        news_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        news_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        news_dialog.setContentView(R.layout.news_dialog);

                        newsDialogImage = news_dialog.findViewById(R.id.imageView_newsPic);
                        tv_newsDialog_title = news_dialog.findViewById(R.id.textView_newsTitle);
                        tv_newsDialog_body = news_dialog.findViewById(R.id.textView_newsBody);
                        tv_newsDialog_seenCount = news_dialog.findViewById(R.id.textView_seenCount);
                        tv_newsDialog_date = news_dialog.findViewById(R.id.textView_date);

                        tv_newsDialog_title.setText(news.getTitle());
                        tv_newsDialog_body.setText(news.getMainText());
                        tv_newsDialog_date.setText(news.getDate());
                        int temp_int = news.getSeen_count()+1;
                        Log.i(TAG, "run: "+temp_int);
                        tv_newsDialog_seenCount.setText(String.valueOf(temp_int));
                        ImageLoader imageLoader;
                        String image_url = serverConstants.ROOT_URL +news.getImage();
                        imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
                        imageLoader.get(image_url, ImageLoader.getImageListener(newsDialogImage, R.drawable.ic_launcher_background, android.R.drawable
                                .ic_dialog_alert));
                        newsDialogImage.setImageUrl(image_url, imageLoader);
                        news_dialog.show();
                    }
                });


            }
        }).start();
    }

    private void getAllNewsFromServer() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String formated_URL = serverConstants.getAllNews_URL;
                    Log.i(TAG, "getAllNewsFromServer: " + formated_URL);
                    URL url = new URL(formated_URL);
                    HttpClient client = new DefaultHttpClient();
                    HttpGet request = new HttpGet();
                    request.setURI(new URI(formated_URL));
                    HttpResponse response = client.execute(request);
                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(response.getEntity().getContent()));
                    StringBuilder sb = new StringBuilder("");
                    String line = "";
                    while ((line = in.readLine()) != null) { sb.append(line); }
                    in.close();
                    String http_get_response = "";
                    http_get_response = sb.toString();

                    // proccess output
                    if (http_get_response.equals("{\"response\":\"No News\"}") || http_get_response.equals("[]")) {
                            Log.i(TAG, "getAllNewsFromServer: No News");
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(context, "هیچ خبری در 10 روز اخیر درج نشده!", Toast.LENGTH_SHORT).show();
                                }
                            });
                    }
                    else {
                        JSONArray jsonArray = new JSONArray(http_get_response);
                        int newsListSize = jsonArray.length()-1;

                        News[] recent_news = new News[newsListSize+1];

                        for (int i=newsListSize; i>=0; i--){
                            News temp = new News();
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            temp.setN_id(jsonObject.getString("n_id"));
                            temp.setTitle(jsonObject.getString("subject"));
                            temp.setTag(jsonObject.getString("tag"));
                            temp.setMainText(jsonObject.getString("body"));
                            temp.setImage(jsonObject.getString("pic"));
                            temp.setSeen_count(jsonObject.getInt("seen"));
                            //TODO: Convert Miladi date to Persian date
                            temp.setDate(convertGdateToJdate(jsonObject.getString("date")));


                            if(jsonObject.getString("tag").equals("kashan")){
                                kashanNewsList.add(temp);
                            }else if(jsonObject.getString("tag").equals("shahrdari")){
                                shahrdariNewsList.add(temp);
                            }
                            recentNewsList.add(temp);

                            recent_news[i] = temp;
                        }
                        Log.i(TAG, "getAllNewsFromServer: News Fetched");

                        // TODO: _DONE_ process mostSeen
                        Arrays.sort(recent_news);
                        for(News test: recent_news){
                            //Log.i(TAG, "run: "+test.getTitle() +": "+test.getSeen_count());
                            mostSeenNewsList.add(test);
                        }

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                // TODO: _DONE_ Update UI here
                                ImageLoader imageLoader;
                                String image_url = serverConstants.ROOT_URL +recentNewsList.get(0).getImage();
                                imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
                                imageLoader.get(image_url, ImageLoader.getImageListener(topNewsImage, R.drawable.ic_launcher_background, android.R.drawable
                                        .ic_dialog_alert));
                                topNewsImage.setImageUrl(image_url, imageLoader);
                                topNewsText.setText(recentNewsList.get(0).getTitle());
                                recentNewsList.remove(0);
                                //_______________
                                recent_adapter.notifyDataSetChanged();
                                mostSeen_adapter.notifyDataSetChanged();
                                kashan_adapter.notifyDataSetChanged();
                                shahrdari_adapter.notifyDataSetChanged();
                            }
                        });
                    }


                } catch (MalformedURLException | URISyntaxException e) {
                    Log.i(TAG, "getMySurveysTitle: MalformedURLException");
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    Log.i(TAG, "getMySurveysTitle: ClientProtocolException");
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.i(TAG, "getMySurveysTitle: IOException");
                    e.printStackTrace();
                } catch (JSONException e) {
                    Log.i(TAG, "getMySurveysTitle: JSONException");
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private String convertGdateToJdate(String Gdate) {
        int year = Integer.valueOf(Gdate.substring(0,4));
        int month = Integer.valueOf(Gdate.substring(5,7));
        int day = Integer.valueOf(Gdate.substring(8));
        DateConverter converter = new DateConverter();
        converter.gregorianToPersian(year, month, day);
        return converter.getYear()+"/"+converter.getMonth()+"/"+converter.getDay();
    }
}
