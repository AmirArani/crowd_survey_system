package com.example.polesystem;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.loopj.android.http.HttpGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;


public class FragmentPoll extends Fragment{

    private static final String TAG = "npq: Poll Fragment";

    private Context context;
    private List<String> tags;
    private List<poll> p;
    private List<String> selected;
    private List<poll> filter;
    private tags_adapt tagList_adapter;
    private surveyList_adaperClass surveyList_adapter;
    private RecyclerView tag_list;
    private ListView survey_list;
    private onclickItem listener;
    private int page;
    private String region;
    private String user_id;


    RadioButton ch1 = null;
    RadioButton ch2=null;
    RadioButton ch3=null;
    RadioButton ch4=null;
    NetworkImageView survey_imageView;
    ImageLoader imageLoader;
    TextView question;
    Button btn_submit;


    public FragmentPoll() {
    }

    public static FragmentPoll newInstance(int page, String region, String user_id) {
        Bundle args = new Bundle();
        args.putInt("page",page);
        args.putString("region",region);
        args.putString("user_id",user_id);
        FragmentPoll fragment = new FragmentPoll();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.poll_fragment,container,false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tag_list = view.findViewById(R.id.tag_list);
        survey_list = view.findViewById(R.id.survey_list);
        tag_list.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false));
        tag_list.setAdapter(tagList_adapter);
        survey_list.setAdapter(surveyList_adapter);
        surveyList_adapter.notifyDataSetChanged();
        // set on click listener for showing survey form
        survey_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                // TODO 3.3: check if user has been participated in this survey or not.
                hasBeenParticipated(filter.get(position).getV_id(),user_id);

                // TODO 3.4: fetch survey information from server
                getSurveyDetail(filter.get(position));
                Dialog q = new Dialog(getActivity());
                q = new Dialog(getActivity());
                q.setContentView(R.layout.question_dialog);

                survey_imageView = q.findViewById(R.id.imageView_surveyPic);
                question = q.findViewById(R.id.textView_newsTitle);
                btn_submit = q.findViewById(R.id.confirm);
                ch1 = q.findViewById(R.id.choice1);
                ch2 = q.findViewById(R.id.choice2);
                ch3 = q.findViewById(R.id.choice3);
                ch4 = q.findViewById(R.id.choice4);
                ch1.setVisibility(View.VISIBLE);
                ch2.setVisibility(View.VISIBLE);
                ch3.setVisibility(View.VISIBLE);
                ch4.setVisibility(View.VISIBLE);
                ch1.setText("در حال دریافت!");
                ch2.setText("در حال دریافت!");
                ch3.setText("در حال دریافت!");
                ch4.setText("در حال دریافت!");

                q.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                q.show();

                final Dialog finalQ = q;
                btn_submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String op = "";
                        if (ch1.isChecked()) {
                            op = ch1.getText().toString();
                        } else if (ch2.isChecked()) {
                            op = ch2.getText().toString();
                        } else if (ch3.isChecked()) {
                            op = ch3.getText().toString();
                        } else if (ch4.isChecked()) {
                            op = ch4.getText().toString();
                        }
                        // TODO 3.5: Submit Vote
                        submitVote(filter.get(position).getV_id(), user_id, op, finalQ);

                    }
                });
            }
        });

        surveyList_adapter.notifyDataSetChanged();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page =  getArguments().getInt("page");
        region = getArguments().getString("region");
        user_id = getArguments().getString("user_id");
        selected = new ArrayList<>();

//        tags = getTags();
        p = getPoll(region, page);
        filter = p;

        context = getContext();

        surveyList_adapter = new surveyList_adaperClass(context,filter,page);

        surveyList_adapter.notifyDataSetChanged();
    }


    List<poll> getPoll(String region, int general){
        List<poll> survey;
        if(general == 1) {
            // TODO 3.1: get general survey
            survey = getSurveyFromServer(region , 1);
        }
        else {
            // TODO 3.2: get regional surveys
            survey = getSurveyFromServer(region , 0);
        }
        return survey;
    }

//    List<String> getTags(){
//        List<String> tags = new ArrayList<>();
//        if(page == 1) {
//            tags.add("ناحیه ۱");
//            tags.add("ناحیه ۲");
//            tags.add("ناحیه ۳");
//            tags.add("ناحیه ۴");
//            tags.add("ناحیه ۵");
//        }else{ }
//        return tags;
//    }

    private List<poll> getSurveyFromServer(final String region, final int general) {
        final List<poll> survey = new ArrayList<>();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String formated_URL = serverConstants.getSurvey_URL+"?generic="+ general +"&region="+ region;
                    Log.i(TAG, "getMySurveysTitle: " + formated_URL);
                    URL url = new URL(formated_URL);
                    HttpClient client = new DefaultHttpClient();
                    HttpGet request = new HttpGet();
                    request.setURI(new URI(formated_URL));

                    HttpResponse response = client.execute(request);
                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(response.getEntity().getContent()));

                    StringBuilder sb = new StringBuilder("");
                    String line = "";

                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                    }

                    in.close();
                    String http_get_response = "";
                    http_get_response = sb.toString();

                    // proccess output
                    if (http_get_response.equals("{\"response\":\"No Vote\"}")) {
                        Log.i(TAG, "getSurveyFromServer: No Survey");
                    } else {
                        JSONArray jsonArray = new JSONArray(http_get_response);
                        for (int i=0; i<jsonArray.length(); i++){
                            poll temp = new poll();
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            temp.setV_id(jsonObject.getString("v_id"));
                            temp.setU_id(jsonObject.getString("u_id"));
                            temp.setTitle(jsonObject.getString("subject"));
                            temp.setGen(String.valueOf(general));
                            temp.setStatus(jsonObject.getString("number"));
                            temp.setPic(jsonObject.getString("pic"));
                            temp.setOp1("Not Fetched!");
                            temp.setOp2("Not Fetched!");
                            temp.setOp3("Not Fetched!");
                            temp.setOp4("Not Fetched!");

                            Log.i(TAG, "run: Survey Title: "+jsonObject.getString("subject"));
                            survey.add(temp);
                        }
                        Log.i(TAG, "getMySurveysTitle: Surveys Title retrieved");
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                surveyList_adapter.notifyDataSetChanged();
                            }
                        });
                    }


                } catch (MalformedURLException | URISyntaxException e) {
                    Log.i(TAG, "getMySurveysTitle: MalformedURLException");
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    Log.i(TAG, "getMySurveysTitle: ClientProtocolException");
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.i(TAG, "getMySurveysTitle: IOException");
                    e.printStackTrace();
                } catch (JSONException e) {
                    Log.i(TAG, "getMySurveysTitle: JSONException");
                    e.printStackTrace();
                }
            }
        }).start();

        return survey;
    }

    private void hasBeenParticipated(final String v_id, final String u_id) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String formated_URL = serverConstants.userInVote_URL+"?u_id="+ u_id +"&v_id="+ v_id;
                    Log.i(TAG, "hasBeenParticipated: " + formated_URL);
                    URL url = new URL(formated_URL);
                    HttpClient client = new DefaultHttpClient();
                    HttpGet request = new HttpGet();
                    request.setURI(new URI(formated_URL));

                    HttpResponse response = client.execute(request);
                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(response.getEntity().getContent()));

                    StringBuilder sb = new StringBuilder("");
                    String line = "";

                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                    }

                    in.close();
                    String http_get_response = "";
                    http_get_response = sb.toString();

                    // proccess output
                    if (http_get_response.equals("{\"response\":\"participate\"}")) {
                        Log.i(TAG, "hasBeenParticipated: participated");
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                btn_submit.setText("نمیتوانید دوباره شرکت کنید");
                                btn_submit.setEnabled(false);
                                btn_submit.setBackgroundResource(R.drawable.signin_dialog_btn3);
                                ch1.setEnabled(false);
                                ch2.setEnabled(false);
                                ch3.setEnabled(false);
                                ch4.setEnabled(false);
                                ch1.setChecked(false);
                            }
                        });
                    } else if (http_get_response.equals("{\"response\":\"did not participate\"}")) {
                        Log.i(TAG, "hasBeenParticipated: did not participated");
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                btn_submit.setText("ثبت");
                                btn_submit.setEnabled(true);
                                ch1.setEnabled(true);
                                ch2.setEnabled(true);
                                ch3.setEnabled(true);
                                ch4.setEnabled(true);
                                ch1.toggle();
                            }
                        });
                    }

                } catch (MalformedURLException | URISyntaxException e) {
                    Log.i(TAG, "hasBeenParticipated: MalformedURLException");
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    Log.i(TAG, "hasBeenParticipated: ClientProtocolException");
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.i(TAG, "hasBeenParticipated: IOException");
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private poll getSurveyDetail(final poll selected_survey) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String formated_URL = serverConstants.getSurveyDetail_URL+"?v_id="+ selected_survey.getV_id();
                    Log.i(TAG, "getSurveyDetail: " + formated_URL);
                    URL url = new URL(formated_URL);
                    HttpClient client = new DefaultHttpClient();
                    HttpGet request = new HttpGet();
                    request.setURI(new URI(formated_URL));

                    HttpResponse response = client.execute(request);
                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(response.getEntity().getContent()));

                    StringBuilder sb = new StringBuilder("");
                    String line = "";

                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                    }

                    in.close();
                    String http_get_response = "";
                    http_get_response = sb.toString();

                    // process output
                    if (http_get_response.equals("{\"response\":\"No Option\"}")) {
                        Log.i(TAG, "getSurveyDetail: No Option");
                    } else {
                        // selected_survey is incomplete. get options and complete it and then return it.
                        JSONArray jsonArray = new JSONArray(http_get_response);
                        if (jsonArray.length() == 2){
                            // 2op
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            String tempS = jsonObject.getString("option");
                            selected_survey.setOp1(tempS);
//                            Log.i(TAG, "getSurveyDetail: op1: "+tempS);
                            jsonObject = jsonArray.getJSONObject(1);
                            tempS = jsonObject.getString("option");
                            selected_survey.setOp2(tempS);
//                            Log.i(TAG, "getSurveyDetail: op2: "+tempS);
                        }else if (jsonArray.length() == 4){
                            // 4op
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            String tempS = jsonObject.getString("option");
                            selected_survey.setOp1(tempS);
//                            Log.i(TAG, "getSurveyDetail: op1: "+tempS);
                            jsonObject = jsonArray.getJSONObject(1);
                            tempS = jsonObject.getString("option");
                            selected_survey.setOp2(tempS);
//                            Log.i(TAG, "getSurveyDetail: op2: "+tempS);
                            jsonObject = jsonArray.getJSONObject(2);
                            tempS = jsonObject.getString("option");
                            selected_survey.setOp3(tempS);
//                            Log.i(TAG, "getSurveyDetail: op3: "+tempS);
                            jsonObject = jsonArray.getJSONObject(3);
                            tempS = jsonObject.getString("option");
                            selected_survey.setOp4(tempS);
//                            Log.i(TAG, "getSurveyDetail: op4: "+tempS);
                        }else{
                            Log.i(TAG, "run: ERROR!!!");
                        }
                    }

                } catch (MalformedURLException | URISyntaxException e) {
                    Log.i(TAG, "getSurveyDetail: MalformedURLException");
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    Log.i(TAG, "getSurveyDetail: ClientProtocolException");
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.i(TAG, "getSurveyDetail: IOException");
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        // TODO 3.6: SHOW PICTURE AND OTHER DATA OF A SURVEY HERE
                        if (selected_survey.getPic().equals("Undefined")){
                            survey_imageView.setVisibility(View.GONE);
                        }else {
                            String image_url = serverConstants.ROOT_URL + selected_survey.getPic();
                            Log.i(TAG, "run: " + image_url);
                            imageLoader = CustomVolleyRequest.getInstance(getContext()).getImageLoader();
                            imageLoader.get(image_url, ImageLoader.getImageListener(survey_imageView, R.drawable.ic_launcher_background, android.R.drawable
                                    .ic_dialog_alert));
                            survey_imageView.setImageUrl(image_url, imageLoader);
                        }

                        question.setText(selected_survey.getTitle());
                        if (selected_survey.getOp3().equals("Not Fetched!") && !selected_survey.getOp1().equals("Not Fetched!")) {
                            ch1.setVisibility(View.VISIBLE);
                            ch2.setVisibility(View.VISIBLE);
                            ch3.setVisibility(View.GONE);
                            ch4.setVisibility(View.GONE);
                            ch1.setText(selected_survey.getOp1());
                            ch2.setText(selected_survey.getOp2());
                        } else if (!selected_survey.getOp3().equals("Not Fetched!") && !selected_survey.getOp1().equals("Not Fetched!")){

                            ch1.setVisibility(View.VISIBLE);
                            ch2.setVisibility(View.VISIBLE);
                            ch3.setVisibility(View.VISIBLE);
                            ch4.setVisibility(View.VISIBLE);
                            ch1.setText(selected_survey.getOp1());
                            ch2.setText(selected_survey.getOp2());
                            ch3.setText(selected_survey.getOp3());
                            ch4.setText(selected_survey.getOp4());
                        }else{
                            ch1.setVisibility(View.VISIBLE);
                            ch2.setVisibility(View.GONE);
                            ch3.setVisibility(View.GONE);
                            ch4.setVisibility(View.GONE);
                            ch1.setText("خطا!");
                        }
                    }
                });
            }
        }).start();

        return selected_survey;
    }

    private void submitVote(final String v_id, final String u_id, final String op, final Dialog finalQ) {
        final Boolean[] submitStatus = {false};
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String formated_URL = serverConstants.voting_URL+"?v_id="+v_id+"&u_id="+u_id+"&option="+op;
                    Log.i(TAG, "submitVote: " + formated_URL);
                    URL url = new URL(formated_URL);
                    HttpClient client = new DefaultHttpClient();
                    HttpGet request = new HttpGet();
                    request.setURI(new URI(formated_URL));
                    HttpResponse response = client.execute(request);
                    BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                    StringBuilder sb = new StringBuilder("");
                    String line = "";
                    while ((line = in.readLine()) != null) { sb.append(line); }
                    in.close();
                    String http_get_response = "";
                    http_get_response = sb.toString();

                    // process output
                    if (http_get_response.equals("{\"response\":\"inserted\"}")) {
                        Log.i(TAG, "submitVote: Inserted");
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getContext(), "نظر شما با موفقیت ثبت شد", Toast.LENGTH_SHORT).show();
                                finalQ.dismiss();
                            }
                        });
                    }else if (http_get_response.equals("{\"response\":\"failed\"}")) {
                        Log.i(TAG, "submitVote: Failed");
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getContext(), "خطا", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }else {
                        Log.i(TAG, "submitVote: ERROR!!!");
                    }

                } catch (MalformedURLException | URISyntaxException e) {
                    Log.i(TAG, "getSurveyDetail: MalformedURLException");
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    Log.i(TAG, "getSurveyDetail: ClientProtocolException");
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.i(TAG, "getSurveyDetail: IOException");
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
