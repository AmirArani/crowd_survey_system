package com.example.polesystem;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.bumptech.glide.Glide;

import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.holder> {

    List<News> neList;
    Context context;


    public NewsAdapter(List<News> neList, Context context) {
        this.neList = neList;
        this.context = context;
    }

    @NonNull
    @Override
    public holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.news_layout,parent,false);
        return new holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull holder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return neList.size();
    }

    public class holder extends RecyclerView.ViewHolder {

        NetworkImageView news_imageView;
        TextView news_text;
        TextView news_date;
        TextView news_clock;

        public holder(@NonNull View itemView) {
            super(itemView);
            news_imageView = itemView.findViewById(R.id.news_image);
            news_text = (TextView) itemView.findViewById(R.id.news_text);
            news_date = (TextView) itemView.findViewById(R.id.news_date);
            news_clock = (TextView) itemView.findViewById(R.id.news_clock);
        }

        void bind(int position){
            // TODO: GET EACH NEWS IMAGE FROM SERVER
            ImageLoader imageLoader;
            String image_url = serverConstants.ROOT_URL + neList.get(position).getImage();
            imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
            imageLoader.get(image_url, ImageLoader.getImageListener(news_imageView, R.drawable.ic_launcher_background, android.R.drawable
                    .ic_dialog_alert));
            news_imageView.setImageUrl(image_url, imageLoader);
            news_text.setText(neList.get(position).getTitle());
            news_date.setText(neList.get(position).getDate());
        }
    }

}
