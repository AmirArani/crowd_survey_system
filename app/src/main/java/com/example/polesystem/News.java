package com.example.polesystem;

public class News implements Comparable<News>{

    private String n_id;
    private String title;
    private String tag;
    private String mainText;
    private String image;
    private String date;
    private String clock;
    private int seen_count;

    public News() {

    }

    public News(String n_id, String title,String tag, String mainText, String image, String date, String clock, int seen_count) {
        this.n_id = n_id;
        this.title = title;
        this.tag = tag;
        this.mainText = mainText;
        this.image = image;
        this.date = date;
        this.clock = clock;
        this.seen_count = seen_count;

    }

    public String getN_id() {
        return n_id;
    }

    public void setN_id(String n_id) {
        this.n_id = n_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getMainText() {
        return mainText;
    }

    public void setMainText(String mainText) {
        this.mainText = mainText;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getClock() {
        return clock;
    }

    public void setClock(String clock) {
        this.clock = clock;
    }

    public int getSeen_count() {
        return seen_count;
    }

    public void setSeen_count(int seen_count) {
        this.seen_count = seen_count;
    }

    @Override
    public int compareTo(News news) {
        int compareQuantity = ((News) news).getSeen_count();

        //ascending order
//        return this.seen_count - compareQuantity;

        //descending order
        return compareQuantity - this.seen_count;
    }
}
