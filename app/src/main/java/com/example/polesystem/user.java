package com.example.polesystem;

public class user {
    private int u_id;
    private String name;
    private String code;
    private String email;
    private String pass;
    private String phone;
    private String u_region;
    private String address;

    public user(int u_id, String name, String code, String email, String pass, String phone, String u_region, String address) {
        this.u_id = u_id;
        this.name = name;
        this.code = code;
        this.email = email;
        this.pass = pass;
        this.phone = phone;
        this.u_region = u_region;
        this.address = address;
    }

    public user(){}

    public int getU_id() {
        return u_id;
    }

    public void setU_id(int u_id) {
        this.u_id = u_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getU_region() {
        return u_region;
    }

    public void setU_region(String u_region) {
        this.u_region = u_region;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
