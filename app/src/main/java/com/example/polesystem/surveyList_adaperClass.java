package com.example.polesystem;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class surveyList_adaperClass extends ArrayAdapter<poll> {

    private Context context;
    private List<poll> p;
    private int page;

    public surveyList_adaperClass(Context context, List<poll> p, int page) {
        super(context, R.layout.qlist, p);
        this.context = context;
        this.p = p;
        this.page = page;
    }

    @NotNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        holder holder;
        if(convertView == null){

            if(page == 1) {
                holder = new holder();
                convertView = LayoutInflater.from(context).inflate(R.layout.qlist2, parent, false);
                holder.title = convertView.findViewById(R.id.title2);
                holder.imageView = convertView.findViewById(R.id.image2);

            }else{
                holder = new holder();
                convertView = LayoutInflater.from(context).inflate(R.layout.qlist, parent, false);
                holder.title = convertView.findViewById(R.id.title);
                holder.progressBar = convertView.findViewById(R.id.progress);
                holder.imageView = convertView.findViewById(R.id.image);

            }
            convertView.setTag(holder);

        }else {
            holder = (holder) convertView.getTag();
        }

        holder.fill(position,p);
        return convertView;
    }

    public class holder{
        TextView title;
        NetworkImageView imageView;
        ImageLoader imageLoader;
        ProgressBar progressBar;

        void fill(int i,List<poll> p){
            title.setText(p.get(i).getTitle());
            String image_url="";
            if (!p.get(i).getPic().equals("Undefined")) {
                image_url = serverConstants.ROOT_URL + p.get(i).getPic();
            }else{
                image_url = serverConstants.ROOT_URL +"image/survey.png";
            }

            imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
            imageLoader.get(image_url, ImageLoader.getImageListener(imageView, R.drawable.ic_launcher_background, android.R.drawable
                    .ic_dialog_alert));
            imageView.setImageUrl(image_url, imageLoader);

            if(page == 2) {
                progressBar.setProgress(Integer.parseInt(p.get(i).getStatus()));
            }
        }
    }
}
