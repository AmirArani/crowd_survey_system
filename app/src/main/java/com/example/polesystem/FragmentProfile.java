package com.example.polesystem;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.HttpGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

public class FragmentProfile extends Fragment {

    private static final String TAG = "npq: Profile Fragment";

    List<String> survey = new ArrayList<String>();;
    ListView list;
    AppCompatImageView btn_profileEdit;
    LinearLayout l2;
    ArrayAdapter<String> arrayAdapter;
    ImageView more;

    private String user = "SS";
    private TextView tv_name;
    private TextView tv_codemeli;
    private TextView tv_address;
    private TextView tv_phone_num;
    private TextView tv_email;
    private TextView tv_participatedNum;
    private TextView tv_madeNum;

    private static String user_id = "";
    private static String user_name = "";
    private static String user_username = "";
    private static String user_lname = "";
    private static String user_codemelli = "";
    private static String user_address = "";
    private static String user_phone = "";
    private static String user_email = "";


    public static FragmentProfile newInstance(String u_id, String username, String name, String lname, String mellicode, String address, String phone, String email) {
        user_id = u_id;
        user_name = name;
        user_username = username;
        user_lname = lname;
        user_codemelli = mellicode;
        user_address = address;
        user_phone = phone;
        user_email = email;

        Bundle args = new Bundle();
        FragmentProfile fragment = new FragmentProfile();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview=inflater.inflate(R.layout.profile_layout,container,false);
        return rootview;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        list=view.findViewById(R.id.list1);
        getMyParticipated_fromServer();

        btn_profileEdit =view.findViewById(R.id.button_profile_edit);
        btn_profileEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), EditProfile.class);
                intent.putExtra("u_id",user_id);
                intent.putExtra("username",user_username);
                startActivityForResult(intent,1);
            }
        });

        l2=view.findViewById(R.id.l2);
        more = view.findViewById(R.id.more);
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!survey.isEmpty()) {
                    if (list.getVisibility() == View.GONE) {
                        l2.animate().translationX(4000f).setDuration(500).start();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                l2.setVisibility(View.GONE);
                            }
                        }, 400);
                        list.setVisibility(View.VISIBLE);
                    } else {
                        list.setVisibility(View.GONE);
                        l2.animate().translationX(0).setDuration(500).start();
                        l2.setVisibility(View.VISIBLE);
                    }
                }
                else{
                    Toast.makeText(getContext(), "مشارکتی یافت نشد", Toast.LENGTH_SHORT).show();
                }
            }

        });

        tv_name = view.findViewById(R.id.textview_profile_name);
        tv_codemeli = view.findViewById(R.id.textview_profile_codemelli);
        tv_address = view.findViewById(R.id.textview_profile_address);
        tv_phone_num = view.findViewById(R.id.textview_profile_phone);
        tv_email = view.findViewById(R.id.textview_profile_email);
        tv_participatedNum = view.findViewById(R.id.textview_profile_participatedNUM);
        tv_madeNum = view.findViewById(R.id.textview_profile_madeNUM);

        tv_name.setText(user_name +" "+ user_lname);
        tv_codemeli.setText(user_codemelli);
        tv_address.setText(user_address);
        tv_phone_num.setText(user_phone);
        tv_email.setText(user_email);


        setMadeNum();

    }


    private void setMadeNum() {
        final String[] temp_count = {"0"};
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String formated_URL = serverConstants.getMySurveyCount_URL+"?u_id="+ user_id;
                    Log.i(TAG, "setMadeNum: " + formated_URL);
                    URL url = new URL(formated_URL);
                    HttpClient client = new DefaultHttpClient();
                    HttpGet request = new HttpGet();
                    request.setURI(new URI(formated_URL));

                    HttpResponse response = client.execute(request);
                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(response.getEntity().getContent()));

                    StringBuilder sb = new StringBuilder("");
                    String line = "";

                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                    }

                    in.close();
                    String http_get_response = "";
                    http_get_response = sb.toString();

                    Log.i(TAG, "run: "+http_get_response);
                    // process output
                    if (http_get_response.equals("{\"response\":\"No Survey\"}")) {
                        Log.i(TAG, "setMadeNum: No Survey");
                    } else {
                        JSONObject jsonObject = new JSONObject(http_get_response);
                        temp_count[0] = jsonObject.getString("numS");
                    }

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tv_madeNum.setText(temp_count[0]);
                        }
                    });

                } catch (MalformedURLException | URISyntaxException e) {
                    Log.i(TAG, "getSurveyDetail: MalformedURLException");
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    Log.i(TAG, "getSurveyDetail: ClientProtocolException");
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.i(TAG, "getSurveyDetail: IOException");
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).start();


    }

    private void getMyParticipated_fromServer() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String formated_URL = serverConstants.getMyParticipate_URL+"?u_id="+ user_id;
                    Log.i(TAG, "getSurveyDetail: " + formated_URL);
                    URL url = new URL(formated_URL);
                    HttpClient client = new DefaultHttpClient();
                    HttpGet request = new HttpGet();
                    request.setURI(new URI(formated_URL));

                    HttpResponse response = client.execute(request);
                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(response.getEntity().getContent()));

                    StringBuilder sb = new StringBuilder("");
                    String line = "";

                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                    }

                    in.close();
                    String http_get_response = "";
                    http_get_response = sb.toString();

                    Log.i(TAG, "run: "+http_get_response);
                    // process output
                    if (http_get_response.equals("{\"response\":\"No Participate\"}")) {
                        Log.i(TAG, "getMyParticipated_fromServer: No Participate");
                    } else {
                        JSONArray jsonArray = new JSONArray(http_get_response);
                        int listSize = jsonArray.length()-1;
                        survey.clear();
                        for (int i=listSize; i>=0; i--){
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String temp_name = jsonObject.getString("subject");
                            survey.add(temp_name);

                        }
                    }

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tv_participatedNum.setText(String.valueOf(survey.size()));
                            arrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, survey);
                            list.setAdapter(arrayAdapter);

                        }
                    });

                } catch (MalformedURLException | URISyntaxException e) {
                    Log.i(TAG, "getSurveyDetail: MalformedURLException");
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    Log.i(TAG, "getSurveyDetail: ClientProtocolException");
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.i(TAG, "getSurveyDetail: IOException");
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

}
