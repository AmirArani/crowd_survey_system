package com.example.polesystem;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.HttpGet;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = "npq: MainActivity";

    private Toolbar toolbar;
    private BottomNavigationView bottomNavigationView;
    private DrawerLayout drawer;
    private NavigationView navigationView;

    public static String user_id = "";
    public static String user_name = "";
    public static String user_lname = "";
    public static String user_email = "";
    public static String user_phone = "";
    public static String user_username = "";
    public static String user_password = "";
    public static String user_region = "";
    public static String user_melliCode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("سامانه نظرسنجی");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        statusBarColor();
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.getDrawerArrowDrawable().setColor(Color.WHITE);
        toggle.syncState();
//        db_main dbMain = new db_main(MainActivity.this);
//        dbMain.insert_user("ali","125051","a@a.com","123456","","ناحیه ۱","kahsan");

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                if(menuItem.getItemId() == R.id.nav_add){
                    if(!user_username.equals("") && !user_password.equals("")) {
                        // TODO 1: _DONE_ Add Survey (POST)
                        Intent intent = new Intent(MainActivity.this, SurveyAdd.class);
                        intent.putExtra("u_id", user_id);
                        intent.putExtra("region", user_region);
                        startActivity(intent);
                    }else {
                        Toast.makeText(MainActivity.this, "لطفا وارد سیستم شوید یا ثبت نام کنید", Toast.LENGTH_SHORT).show();
                    }
                }

                if (menuItem.getItemId() == R.id.nav_emergency){
                    final Dialog emergency_dialog = new Dialog(MainActivity.this);
                    emergency_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    emergency_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    emergency_dialog.setContentView(R.layout.emergency_dialog);
                    emergency_dialog.show();
                }

                if(menuItem.getItemId() == R.id.nav_send){
                    final Dialog contact_dialog = new Dialog(MainActivity.this);
                    contact_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    contact_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    contact_dialog.setContentView(R.layout.contact_dialog);
                    contact_dialog.show();
                }

                if (menuItem.getItemId() == R.id.nav_exit){
                    if(!user_username.equals("") && !user_password.equals("")) {
                        AlertDialog.Builder d = new AlertDialog.Builder(MainActivity.this);
                        d.setMessage("آیا مایل به خروج از حساب خود هستید؟");
                        d.setPositiveButton("تایید", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                user_username = "";
                                user_password = "";
                                user_region = "";
                            }
                        });
                        d.show();
                    }else{
                        Toast.makeText(MainActivity.this, "ابتدا وارد شوید", Toast.LENGTH_SHORT).show();
                    }
                }

                if(menuItem.getItemId() == R.id.nav_delete){
                    if(!user_username.equals("") && !user_password.equals("")) {
                        Intent intent = new Intent(MainActivity.this, SurveyDelete.class);
                        // TODO 2: _DONE_ deleteVote (GET)
                        intent.putExtra("u_id", user_id);
                        startActivity(intent);
                    }else {
                        Toast.makeText(MainActivity.this, "لطفا وارد سیستم شوید یا ثبت نام کنید", Toast.LENGTH_SHORT).show();
                    }
                }
                return false;
            }
        });

        FragmentNews show = new FragmentNews();
        getSupportFragmentManager().beginTransaction().replace(R.id.container,show).commit();
        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                if(menuItem.getItemId() == R.id.survey_all){
                    if(user_region.equals("")) {
                        Toast.makeText(MainActivity.this, "لطفا وارد شوید یا ثبت نام کنید", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        //TODO 3: _DONE_ show all surveys
                        FragmentPoll fragment = FragmentPoll.newInstance(1, user_region, user_id);
                        Log.i(TAG, "onNavigationItemSelected: Go to PollFragment");
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
                    }
                }
                if(menuItem.getItemId() == R.id.survey_regional){
                    if(user_region.equals("")){
                        Toast.makeText(MainActivity.this, "لطفا ناحیه خود را مشخص کنید یا وارد شوید", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        //TODO 4: _DONE_ show regional surveys
                        FragmentPoll fragment = FragmentPoll.newInstance(2, user_region, user_id);
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
                    }
                }
                if(menuItem.getItemId() == R.id.news){
                    // TODO 5: show news
                    FragmentNews news = new FragmentNews();
                    getSupportFragmentManager().beginTransaction().replace(R.id.container,news).commit();
                }
                if(menuItem.getItemId() == R.id.profile){
                    if(user_username.equals("") && user_password.equals("")){
                        final Dialog dialog = new Dialog(MainActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.setContentView(R.layout.sign_dialog);
                        dialog.show();

                        final EditText user = dialog.findViewById(R.id.editText_signin_dialog_user);
                        final EditText pass = dialog.findViewById(R.id.editText_signin_dialog_pass);
                        Button signup = dialog.findViewById(R.id.btn_signup);
                        Button signin = dialog.findViewById(R.id.btn_signin);

                        signin.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                user_username = user.getText().toString().trim();
                                user_password = pass.getText().toString().trim();

                                // TODO 6: _DONE_ userAuth (GET)
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            String formated_URL = serverConstants.SignIn_URL+"?username="+ user_username +"&password="+ user_password;

                                            URL url = new URL(formated_URL);
                                            HttpClient client = new DefaultHttpClient();
                                            HttpGet request = new HttpGet();
                                            request.setURI(new URI(formated_URL));
                                            HttpResponse response = client.execute(request);

                                            BufferedReader in = new BufferedReader(new
                                               InputStreamReader(response.getEntity().getContent()));

                                            StringBuffer sb = new StringBuffer("");
                                            String line="";

                                            while ((line = in.readLine()) != null)  sb.append(line);

                                            in.close();
                                            String http_get_response = "";
                                            http_get_response = sb.toString();

                                            JSONObject signin_obj = new JSONObject(http_get_response); //convert string to json object

                                            // proccess output
                                            String get_response = signin_obj.getString("response");

                                            if (get_response.equals("user is valid")) {
                                                Log.i(TAG, "SignIn: run: Valid User");

                                                String get_userID = signin_obj.getString("u_id");
                                                String get_name = signin_obj.getString("name");
                                                String get_lname = signin_obj.getString("lname");
                                                String get_mellicode = signin_obj.getString("meli");
                                                String get_phone = signin_obj.getString("phone");
                                                String get_email = signin_obj.getString("email");
                                                String get_region = signin_obj.getString("region");
                                                String get_username = signin_obj.getString("usern");
                                                String get_password = signin_obj.getString("passw");

                                                user_id = get_userID;
                                                user_name = get_name;
                                                user_lname = get_lname;
                                                user_melliCode = get_mellicode;
                                                user_phone = get_phone;
                                                user_email = get_email;
                                                user_username = get_username;
                                                user_password = get_password;
                                                user_region = get_region;

                                                FragmentProfile profile = FragmentProfile.newInstance(user_id, user_username, user_name, user_lname, user_melliCode, user_region, user_phone, user_email);
                                                getSupportFragmentManager().beginTransaction().replace(R.id.container, profile).commit();

                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        dialog.dismiss();
                                                    }
                                                });


                                            }
                                            else if (get_response.equals("invalid user")) {
                                                Log.i(TAG, "SignIn: run: INValid User");
                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        user.setText("");
                                                        user.requestFocus();
                                                        pass.setText("");
                                                        user_username = "";
                                                        user_password = "";
                                                    }
                                                });
                                            }


                                        }
                                        catch (MalformedURLException | URISyntaxException e) {
                                            Log.i(TAG, "SignIn: run: MalformedURLException");
                                            e.printStackTrace();
                                        } catch (ClientProtocolException e) {
                                            Log.i(TAG, "SignIn: run: ClientProtocolException");
                                            e.printStackTrace();
                                        } catch (IOException e) {
                                            Log.i(TAG, "SignIn: run: IOException");
                                            e.printStackTrace();
                                        } catch (JSONException e) {
                                            Log.i(TAG, "SignIn: run: JSONException");
                                            e.printStackTrace();
                                        }
                                    }
                                }).start();
                            }
                        });
                        signup.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(new Intent(MainActivity.this,SignUp.class));
                            }
                        });
                    }
                    else {
                        FragmentProfile fragment_profile = FragmentProfile.newInstance(user_id, user_username, user_name, user_lname, user_melliCode, user_region, user_phone, user_email);
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment_profile).commit();
                    }
                }
                return false;
            }
        });
    }

    void statusBarColor(){
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP){
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }
    }
}
