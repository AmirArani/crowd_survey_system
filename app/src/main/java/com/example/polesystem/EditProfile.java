package com.example.polesystem;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.HttpGet;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

//user Auth -> if ok -> send u_id and

public class EditProfile extends AppCompatActivity {

    private static final String TAG = "npq";
    private EditText current_password;
    private EditText new_password;
    private EditText new_phone;
    private EditText new_email;
    private String u_id;
    private String u_name;

    private Button conf, delete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile);
        u_id = getIntent().getStringExtra("u_id");
        u_name = getIntent().getStringExtra("username");

        Log.i(TAG, "onCreate: "+u_id);
        Log.i(TAG, "onCreate: "+u_name);

        current_password = findViewById(R.id.edit_current_password);
        new_password = findViewById(R.id.edit_new_password);
        new_phone = findViewById(R.id.edit_phone);
        new_email = findViewById(R.id.edit_mail);
        conf = findViewById(R.id.conf);
        delete = findViewById(R.id.deleteProfile);

        conf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //auth
                        try {
                            String formated_URL = serverConstants.SignIn_URL+"?username="+ u_name +"&password="+ current_password.getText().toString().trim();
                            URL url = new URL(formated_URL);
                            HttpClient client = new DefaultHttpClient();
                            HttpGet request = new HttpGet();
                            request.setURI(new URI(formated_URL));
                            HttpResponse response = client.execute(request);
                            BufferedReader in = new BufferedReader(new
                                    InputStreamReader(response.getEntity().getContent()));
                            StringBuffer sb = new StringBuffer("");
                            String line="";
                            while ((line = in.readLine()) != null) { sb.append(line); }
                            in.close();
                            String http_get_response = "";
                            http_get_response = sb.toString();
                            JSONObject signin_obj = new JSONObject(http_get_response); //convert string to json object
                            // proccess output
                            String get_response = signin_obj.getString("response");
                            if (get_response.equals("user is valid")) {
                                Log.i(TAG, "edit : run: Valid User");

                                String new_passwords = new_password.getText().toString().trim();
                                String new_emails = new_email.getText().toString().trim();
                                String new_phones = new_phone.getText().toString().trim();

                                if(new_passwords.equals("")){new_passwords="unchanged";}
                                if(new_emails.equals("")){new_emails="unchanged";}
                                if(new_phones.equals("")){new_phones="unchanged";}

                                Log.i(TAG, "run: "+new_passwords +" "+new_emails+" "+new_phones);

                                String data2 = URLEncoder.encode("u_id", "UTF-8") + "=" +
                                        URLEncoder.encode(u_id, "UTF-8");
                                data2 += "&" + URLEncoder.encode("password", "UTF-8") + "=" +
                                        URLEncoder.encode(new_passwords, "UTF-8");
                                data2 += "&" + URLEncoder.encode("phone", "UTF-8") + "=" +
                                        URLEncoder.encode(new_phones, "UTF-8");
                                data2 += "&" + URLEncoder.encode("email", "UTF-8") + "=" +
                                        URLEncoder.encode(new_emails, "UTF-8");

                                URL url2 = new URL(serverConstants.editProfile_URL);
                                URLConnection conn2 = url2.openConnection();

                                conn2.setDoOutput(true);
                                OutputStreamWriter wr2 = new OutputStreamWriter(conn2.getOutputStream());

                                wr2.write( data2 );
                                wr2.flush();

                                BufferedReader reader2 = new BufferedReader(new InputStreamReader(conn2.getInputStream()));

                                final StringBuilder sb2 = new StringBuilder();
                                String line2;

                                // Read Server Response
                                while((line2 = reader2.readLine()) != null) {
                                    sb2.append(line2);
                                }

                                System.out.println(sb2.toString());
                                Log.i(TAG, "callAPI2: "+sb2.toString());

                                if (sb2.toString().equals("{\"response\":\"user info edited\"}")){
                                    Log.i(TAG, "run: user info edited");
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(EditProfile.this, "اطلاعات با موفقیت ویرایش شد!", Toast.LENGTH_SHORT).show();
                                        }
                                    });

                                }else if (sb2.toString().equals("{\"response\":\"nothing changed!\"}")){
                                    Log.i(TAG, "run: nothing changed!");
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(EditProfile.this, "چیزی را تعییر نداده اید!", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }

                            }

                            else if (get_response.equals("invalid user")) {
                                Log.i(TAG, "SignIn: run: INValid User");
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(EditProfile.this, "رمز عبور فعلی اشتباه وارد شده!", Toast.LENGTH_LONG).show();
                                    }
                                });
                            }


                        }
                        catch (MalformedURLException | URISyntaxException e) {
                            Log.i(TAG, "SignIn: run: MalformedURLException");
                            e.printStackTrace();
                        } catch (ClientProtocolException e) {
                            Log.i(TAG, "SignIn: run: ClientProtocolException");
                            e.printStackTrace();
                        } catch (IOException e) {
                            Log.i(TAG, "SignIn: run: IOException");
                            e.printStackTrace();
                        } catch (JSONException e) {
                            Log.i(TAG, "SignIn: run: JSONException");
                            e.printStackTrace();
                        }

                    }
                }).start();
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //auth
                        try {
                            String formated_URL = serverConstants.SignIn_URL+"?username="+ u_name +"&password="+ current_password.getText().toString().trim();
                            URL url = new URL(formated_URL);
                            HttpClient client = new DefaultHttpClient();
                            HttpGet request = new HttpGet();
                            request.setURI(new URI(formated_URL));
                            HttpResponse response = client.execute(request);
                            BufferedReader in = new BufferedReader(new
                                    InputStreamReader(response.getEntity().getContent()));
                            StringBuffer sb = new StringBuffer("");
                            String line="";
                            while ((line = in.readLine()) != null) { sb.append(line); }
                            in.close();
                            String http_get_response = "";
                            http_get_response = sb.toString();
                            JSONObject signin_obj = new JSONObject(http_get_response); //convert string to json object
                            // proccess output
                            String get_response = signin_obj.getString("response");
                            if (get_response.equals("user is valid")) {

                                //delete User
                                String formated_URL2 = serverConstants.deleteUser_URL+"?username="+ u_name +"&password="+ current_password.getText().toString().trim();
                                URL url2 = new URL(formated_URL2);
                                HttpClient client2 = new DefaultHttpClient();
                                HttpGet request2 = new HttpGet();
                                request2.setURI(new URI(formated_URL2));
                                HttpResponse response2 = client2.execute(request2);
                                BufferedReader in2 = new BufferedReader(new
                                        InputStreamReader(response2.getEntity().getContent()));

                                StringBuffer sb2 = new StringBuffer("");
                                String line2="";
                                while ((line2 = in2.readLine()) != null) { sb2.append(line2); }
                                in2.close();
                                String http_get_response2 = "";
                                http_get_response2 = sb2.toString();
                                JSONObject signin_obj2 = new JSONObject(http_get_response2); //convert string to json object
                                // proccess output
                                String get_response2 = signin_obj2.getString("response");
                                if (get_response2.equals("user deleted")) {
                                    Log.i(TAG, "SignIn: run: INValid User");
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(EditProfile.this, "حساب کاربری حذف شد!", Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }

                                else if (get_response2.equals("Not Find Users")) {
                                    Log.i(TAG, "SignIn: run: INValid User");
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(EditProfile.this, "خظا!", Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            }

                            else if (get_response.equals("invalid user")) {
                                Log.i(TAG, "SignIn: run: INValid User");
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(EditProfile.this, "رمز عبور فعلی اشتباه وارد شده!", Toast.LENGTH_LONG).show();
                                    }
                                });
                            }


                        }
                        catch (MalformedURLException | URISyntaxException e) {
                            Log.i(TAG, "SignIn: run: MalformedURLException");
                            e.printStackTrace();
                        } catch (ClientProtocolException e) {
                            Log.i(TAG, "SignIn: run: ClientProtocolException");
                            e.printStackTrace();
                        } catch (IOException e) {
                            Log.i(TAG, "SignIn: run: IOException");
                            e.printStackTrace();
                        } catch (JSONException e) {
                            Log.i(TAG, "SignIn: run: JSONException");
                            e.printStackTrace();
                        }

                    }
                }).start();
            }
        });

    }
}
