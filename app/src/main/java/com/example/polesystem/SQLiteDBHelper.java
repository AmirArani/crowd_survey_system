package com.example.polesystem;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;

public class SQLiteDBHelper extends SQLiteOpenHelper {
    private static final String TAG = "npq: DBH: ";
    private static final String db_name = "GeolocationDB";
    private static final String table_name = "GeolocationTable";
    private static final int db_version = 1;

    private static final String QUERY_CREATE_TABLE = "CREATE TABLE IF NOT EXISTS '"+table_name+"' " +
            "('city' TEXT NOT NULL," +
            "'street' TEXT NOT NULL," +
            "'area' TEXT NOT NULL)";


    public SQLiteDBHelper(Context context) {
        super(context, db_name, null, db_version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(QUERY_CREATE_TABLE);
        Log.i(TAG, "onCreate: "+QUERY_CREATE_TABLE);
        Log.i(TAG, "onCreate: Table Created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

    public String getAreafromDB(String city, String street){
        String area = "یافت نشد!!!";

        String QUERY_FIND_AREA = "SELECT area FROM '"+table_name +
                                 "' WHERE city = '"+city+"' AND street = '"+street+"' ;";

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(QUERY_FIND_AREA, null);

        if (cursor.moveToFirst())
            area = cursor.getString(cursor.getColumnIndex("area"));

        if (db.isOpen())  db.close();

        Log.i(TAG, "getAreafromDB: AREA: "+area);
        return area;
    }

    public void insertAreaToDB(){
        SQLiteDatabase db1 = getReadableDatabase();
        String QUERY_CHECK = "SELECT * FROM "+table_name;

        Cursor cursor = db1.rawQuery(QUERY_CHECK, null);
        if (!cursor.moveToFirst()) {

            SQLiteDatabase db = getWritableDatabase();

            ContentValues values1 = new ContentValues();
            ContentValues values2 = new ContentValues();
            ContentValues values3 = new ContentValues();

            values1.put("city", "Kashan");
            values1.put("street", "بلوار علامه قطب رواندی");
            values1.put("area", "ناحیه5");
            db.insert(table_name, null, values1);

            values2.put("city", "Kashan");
            values2.put("street", "Amir Kabir");
            values2.put("area", "ناحیه1");
            db.insert(table_name, null, values2);

            values3.put("city", "Kashan");
            values3.put("street", "شهید رجایی");
            values3.put("area", "ناحیه3");
            db.insert(table_name, null, values3);

            if (db.isOpen()) db.close();
        }

        if (db1.isOpen()) db1.close();
    }
}
