package com.example.polesystem;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.loopj.android.http.HttpGet;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

public class SurveyDelete extends AppCompatActivity {

    private static final String TAG = "npq: Delete Survey";

    private ListView addList;
    private List<String> user_surveyName_list;
    private List<String> user_surveyID_list;
    private ArrayAdapter<String> adapter;
    private String user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.delete_page);

        addList = findViewById(R.id.addList);

        user_id = getIntent().getStringExtra("u_id");
        Log.i(TAG, "onCreate: User ID: " + user_id);


        //  TODO 2.1: _DONE_ getMySurvey (GET)
        user_surveyName_list = getMySurveysTitle();
        user_surveyID_list = getMySurveysID();

        adapter = new ArrayAdapter<>(SurveyDelete.this, android.R.layout.simple_list_item_1, user_surveyName_list);
        addList.setAdapter(adapter);

        addList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(SurveyDelete.this);
                dialog.setMessage("آیا می خواهید این نظرسنجی را پاک کنید؟");
                dialog.setPositiveButton("تایید", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.i(TAG, "onCreate: onClick: Pos: "+position+"\tV_id: "+user_surveyID_list.get(position));
                        // TODO 2.2: _DONE_ deleteVote (GET)
                        deleteSurvey(user_surveyID_list.get(position));
                        user_surveyName_list.remove(position);
                        Log.i(TAG, "onCreate: onClick: deleted!");
                        adapter.notifyDataSetChanged();
                    }
                });
                dialog.setCancelable(true);
                dialog.show();
            }
        });
    }

    @NotNull
    private List<String> getMySurveysTitle() {
        final List<String> tempSurveyName = new ArrayList<>();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String formated_URL = serverConstants.getMySurvey_URL + "?u_id=" + user_id;
                    //Log.i(TAG, "getMySurveysTitle: " + formated_URL);
                    URL url = new URL(formated_URL);
                    HttpClient client = new DefaultHttpClient();
                    HttpGet request = new HttpGet();
                    request.setURI(new URI(formated_URL));

                    HttpResponse response = client.execute(request);
                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(response.getEntity().getContent()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                    }

                    in.close();
                    String http_get_response = "";
                    http_get_response = sb.toString();

                    // proccess output
                    if (http_get_response.equals("{\"response\":\"No Vote\"}")) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(SurveyDelete.this, "شما هیچ نظزسنجی تا کنون اضافه نکرده اید.", Toast.LENGTH_SHORT).show();
                            }
                        });
                        Log.i(TAG, "getMySurveysTitle: No Survey");
                    } else {
                        JSONArray jsonArray = new JSONArray(http_get_response);
                        for (int i=0; i<jsonArray.length(); i++){
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            tempSurveyName.add(jsonObject.getString("subject"));
                        }
                        Log.i(TAG, "getMySurveysTitle: Surveys Title retrieved");
                    }


                } catch (MalformedURLException | URISyntaxException e) {
                    Log.i(TAG, "getMySurveysTitle: MalformedURLException");
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    Log.i(TAG, "getMySurveysTitle: ClientProtocolException");
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.i(TAG, "getMySurveysTitle: IOException");
                    e.printStackTrace();
                } catch (JSONException e) {
                    Log.i(TAG, "getMySurveysTitle: JSONException");
                    e.printStackTrace();
                }
            }
        }).start();

        return tempSurveyName;
    }

    @NotNull
    private List<String> getMySurveysID() {
        final List<String> tempSurveyID = new ArrayList<>();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String formated_URL = serverConstants.getMySurvey_URL + "?u_id=" + user_id;
                    Log.i(TAG, "getMySurveysID: " + formated_URL);
                    URL url = new URL(formated_URL);
                    HttpClient client = new DefaultHttpClient();
                    HttpGet request = new HttpGet();
                    request.setURI(new URI(formated_URL));

                    HttpResponse response = client.execute(request);
                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(response.getEntity().getContent()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                    }

                    in.close();
                    String http_get_response = "";
                    http_get_response = sb.toString();

                    // proccess output
                    if (http_get_response.equals("{\"response\":\"No Vote\"}")) {
                        Log.i(TAG, "getMySurveysID: No Survey");
                    } else {
                        JSONArray jsonArray = new JSONArray(http_get_response);
                        for (int i=0; i<jsonArray.length(); i++){
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            tempSurveyID.add(jsonObject.getString("v_id"));
                        }
                        Log.i(TAG, "getMySurveysID: Surveys ID retrieved");
                    }


                } catch (MalformedURLException | URISyntaxException e) {
                    Log.i(TAG, "getMySurveysID: MalformedURLException");
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    Log.i(TAG, "getMySurveysID: ClientProtocolException");
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.i(TAG, "getMySurveysID: IOException");
                    e.printStackTrace();
                } catch (JSONException e) {
                    Log.i(TAG, "getMySurveysID: JSONException");
                    e.printStackTrace();
                }
            }
        }).start();

        return tempSurveyID;
    }

    private void deleteSurvey(final String v_id) {
        final boolean[] bool = {false};

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String formated_URL = serverConstants.deleteSurvey_URL + "?v_id=" + v_id;
                    Log.i(TAG, "deleteSurvey: " + formated_URL);
                    URL url = new URL(formated_URL);
                    HttpClient client = new DefaultHttpClient();
                    HttpGet request = new HttpGet();
                    request.setURI(new URI(formated_URL));

                    HttpResponse response = client.execute(request);
                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(response.getEntity().getContent()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                    }

                    in.close();
                    String http_get_response = "";
                    http_get_response = sb.toString();

                    // proccess output
                    if (http_get_response.equals("{\"response\":\"deleted\"}")) {
                        Log.i(TAG, "deleteSurvey: survey deleted");
                        bool[0] = true;
                    } else if (http_get_response.equals("{\"response\":\"No Vote\"}")) {
                        Log.i(TAG, "deleteSurvey: cant delete survey");
                    } else {
                        Log.i(TAG, "deleteSurvey: ERROR!!!");
                    }


                } catch (MalformedURLException | URISyntaxException e) {
                    Log.i(TAG, "getMySurveysID: MalformedURLException");
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    Log.i(TAG, "getMySurveysID: ClientProtocolException");
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.i(TAG, "getMySurveysID: IOException");
                    e.printStackTrace();
                }
            }
        }).start();

    }

}
