package com.example.polesystem;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class tags_adapt extends RecyclerView.Adapter<tags_adapt.Holder> {

    private Context context;
    private List<String> tags;
    private onclickItem onclick;
    public tags_adapt(Context context,List<String> tags,onclickItem onclick) {
        this.context = context;
        this.tags = tags;
        this.onclick = onclick;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup,int i) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.tags,viewGroup,false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {

        holder.tag_text.setText(tags.get(i));
    }

    @Override
    public int getItemCount() {
        return tags.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        private TextView tag_text;
        public Holder(@NonNull final View itemView) {
            super(itemView);
            tag_text = itemView.findViewById(R.id.tag_text);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextView tv = itemView.findViewById(R.id.tag_text);
                    tv.setTextColor(Color.WHITE);
                    tv.setBackgroundResource(R.drawable.background2);
                    onclick.onItemClick(getAdapterPosition());
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    TextView tv = itemView.findViewById(R.id.tag_text);
                    tv.setTextColor(Color.BLACK);
                    tv.setBackgroundResource(R.drawable.background);
                    onclick.onLongItemClick(getAdapterPosition());
                    return true;
                }
            });

        }
    }


}

interface onclickItem {

    void onItemClick(int possition);
    void onLongItemClick(int possition);

}
