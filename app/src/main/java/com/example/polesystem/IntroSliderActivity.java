package com.example.polesystem;


import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.badoualy.stepperindicator.StepperIndicator;

public class IntroSliderActivity extends AppCompatActivity {

    ViewPager viewPager;
    LinearLayout layoutDots;
    Button btnNext, btnSkip;

    StepperIndicator indicator;

    SliderPagerAdapter pagerAdapter;


    private SliderPrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_slider);

        changeStatusBarColor();

        prefManager = new SliderPrefManager(this);
        if (!prefManager.startSider()){
            lunchMainActivity();
            finish();
        }

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        layoutDots = (LinearLayout) findViewById(R.id.layout_dots);
        btnNext = (Button) findViewById(R.id.btn_next);
        btnSkip = (Button) findViewById(R.id.btn_skip);

        indicator = findViewById(R.id.indiactor);

        pagerAdapter = new SliderPagerAdapter();
        viewPager.setAdapter(pagerAdapter);

        indicator.setViewPager(viewPager);
        indicator.setViewPager(viewPager, viewPager.getAdapter().getCount() +1);



        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


            }

            @Override
            public void onPageSelected(int position) {

                if (position == viewPager.getAdapter().getCount()-1){
                    btnSkip.setVisibility(View.GONE);
                    btnNext.setText(R.string.btn_gotit);
                }else{
                    btnSkip.setVisibility(View.VISIBLE);
                    btnNext.setText(R.string.btn_next);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentPage = indicator.getCurrentStep();
                if (currentPage == viewPager.getAdapter().getCount()-1){
                    prefManager.setStartSlider(false);
                    lunchMainActivity();
                }else{
                    viewPager.setCurrentItem(currentPage+1);
                }

            }
        });

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prefManager.setStartSlider(false);
                lunchMainActivity();
            }
        });

    }

    private void lunchMainActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    //first we should write an adapter for view Pager
    public class SliderPagerAdapter extends PagerAdapter {

        String[] slideTitles;
        String[] slideDescs;
        int[] bgColorIds = {R.color.slide_1_bg_color, R.color.slide_2_bg_color, R.color.slide_3_bg_color, R.color.slide_4_bg_color };
        int[] slideImageIds = {R.drawable.i1, R.drawable.i2, R.drawable.i3, R.drawable.i4};

        public SliderPagerAdapter(){
            slideTitles = getResources().getStringArray(R.array.slide_titles);
            slideDescs = getResources().getStringArray(R.array.slide_descs);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = LayoutInflater.from(IntroSliderActivity.this).inflate(R.layout.slide ,container,false);

            view.findViewById(R.id.bgLayout).setBackgroundColor(ContextCompat.getColor(IntroSliderActivity.this, bgColorIds[position]));
            ((ImageView) view.findViewById(R.id.slide_image)).setImageResource(slideImageIds[position]);
            ((TextView) view.findViewById(R.id.slide_title)).setText(slideTitles[position]);
            ((TextView) view.findViewById(R.id.slide_desc)).setText(slideDescs[position]);

            container.addView(view);

            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;

            container.removeView(view);
        }

        //get slide numbers
        @Override
        public int getCount() {
            return bgColorIds.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }

    private void changeStatusBarColor(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Window window = getWindow();
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }
}
