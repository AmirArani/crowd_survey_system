package com.example.polesystem;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class SurveyAdd extends AppCompatActivity {

    private static final String TAG = "npq: Add Survey:";

    TextInputLayout inputLayout_op3;
    TextInputLayout inputLayout_op4;

    Uri imageUri;

    private EditText et_subject;
    private EditText et_op1;
    private EditText et_op2;
    private EditText et_op3;
    private EditText et_op4;
    private CheckBox cb_4op;
    private CheckBox cb_general;
    private CheckBox cb_pic;
    private Button btn_submit;
    private Button btn_addpic;
    private String u_id;
    private String region;
    ImageView iv_surveyPic;
    Boolean isPicAdded = false;
    int general = 0;
    String subject = "";
    String opt1 = "";
    String opt2 = "";
    String opt3 = "";
    String opt4 = "";
    String encoded_pic="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_survey_dialog);

        u_id = getIntent().getStringExtra("u_id");
        region = getIntent().getStringExtra("region");

        Log.i(TAG, "onCreate: user data: u_id: "+u_id+"\tregion: "+region);

        et_subject = findViewById(R.id.edittext_subject);
        et_op1 = findViewById(R.id.edittext_op1);
        et_op2 = findViewById(R.id.edittext_op2);
        et_op3 = findViewById(R.id.edittext_op3);
        et_op4 = findViewById(R.id.edittext_op4);
        cb_general = findViewById(R.id.checkbox_addsurvey_general);
        cb_4op = findViewById(R.id.checkbox_addsurvey_4op);
        cb_pic = findViewById(R.id.checkbox_addsurvey_pic);
        btn_submit = findViewById(R.id.button_addsurvey_submit);
        btn_addpic = findViewById(R.id.button_addsurvey_addpic);
        inputLayout_op3 = findViewById(R.id.textinputlayout4_op3);
        inputLayout_op4 = findViewById(R.id.textinputlayout5_op4);
        iv_surveyPic = findViewById(R.id.imageView_survey_pic);


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subject = et_subject.getText().toString().trim();
                opt1 = et_op1.getText().toString().trim();
                opt2 = et_op2.getText().toString().trim();
                opt3 = et_op3.getText().toString().trim();
                opt4 = et_op4.getText().toString().trim();


                if (cb_general.isChecked()){ general = 1;}
                else { general = 0;}


                if (cb_pic.isChecked()){
                    if (isPicAdded){
                        if (cb_4op.isChecked()){
                            if (!subject.equals("")&&!opt1.equals("")&&!opt2.equals("")&&!opt3.equals("")&&!opt4.equals("")) {
                                addSurveyAPI4opPIC();   // 4op pic api
                                finish();
                            }
                            else {
                                Toast.makeText(SurveyAdd.this, "لطفا تمام فیلد ها را پر کنید!", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            if (!subject.equals("")&&!opt1.equals("")&&!opt2.equals("")) {
                                addSurveyAPI2opPIC();   // 2op pic api
                                finish();
                            }else {
                                Toast.makeText(SurveyAdd.this, "لطفا تمام فیلد ها را پر کنید!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }else{
                        // error. pic not uploaded
                        Toast.makeText(SurveyAdd.this, "لطفا عکس را ضمیمه کنید!", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    if (cb_4op.isChecked()){
                        if (!subject.equals("")&&!opt1.equals("")&&!opt2.equals("")&&!opt3.equals("")&&!opt4.equals("")) {
                            addSurveyAPI4op();          // 4op api
                            finish();
                        }else {
                            Toast.makeText(SurveyAdd.this, "لطفا تمام فیلد ها را پر کنید!", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        if (!subject.equals("")&&!opt1.equals("")&&!opt2.equals("")) {
                            addSurveyAPI2op();          // 2op api
                            finish();
                        }else {
                            Toast.makeText(SurveyAdd.this, "لطفا تمام فیلد ها را پر کنید!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });


        btn_addpic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallery();
            }
        });

        cb_4op.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    inputLayout_op3.setVisibility(View.VISIBLE);
                    inputLayout_op4.setVisibility(View.VISIBLE);
                }else{
                    inputLayout_op3.setVisibility(View.GONE);
                    inputLayout_op4.setVisibility(View.GONE);
                }
            }
        });

        cb_pic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    btn_addpic.setVisibility(View.VISIBLE);
                    iv_surveyPic.setVisibility(View.VISIBLE);
                }else{
                    btn_addpic.setVisibility(View.GONE);
                    iv_surveyPic.setVisibility(View.GONE);
                }
            }
        });


    }


    private void openGallery() {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, 100);   //100 FOR PICK IMAGE
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 100){
            // select pic
            imageUri = data.getData();
            Log.i(TAG, "onActivityResult: imageURI: "+imageUri);
            iv_surveyPic.setImageURI(imageUri);

            // find pic location
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(imageUri,
                    filePathColumn, null, null, null);
            if (cursor == null) throw new AssertionError();
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            Log.i(TAG, "onActivityResult: picturePath: "+picturePath);

            // resize input photo
            Bitmap orginal_bitmap = BitmapFactory.decodeFile(picturePath);
            int width = orginal_bitmap.getWidth();
            int height = orginal_bitmap.getHeight();
            Log.i(TAG, "onActivityResult: Original Size: "+width+" x "+height);
//            Matrix matrix = new Matrix();
//            float scaleWidth = ((float) 200) / width;       //200 -> desired size
//            float scaleHeight = ((float) 200) / height;
//            matrix.postScale(scaleWidth, scaleHeight);
//            Bitmap resized_bitmap = Bitmap.createBitmap(orginal_bitmap, 0, 0, width, height, matrix, true);
            Bitmap resized_bitmap = Bitmap.createScaledBitmap(orginal_bitmap, 200, 200, true);
//            Bitmap resized_bitmap = Bitmap.createScaledBitmap(orginal_bitmap,(int)(orginal_bitmap.getWidth()*0.8), (int)(orginal_bitmap.getHeight()*0.8), true);


            // convert it to base64 string
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            resized_bitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);
            byte[] bytes = outputStream.toByteArray();
            encoded_pic = Base64.encodeToString(bytes, Base64.DEFAULT);
            //Log.i(TAG, "onActivityResult: "+encoded_pic);


            isPicAdded = true;
        }
        else{
            isPicAdded = false;
        }
    }


    // TODO 1.1: _DONE_ addSurvey finctions (POST)
    private void addSurveyAPI2op() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String data = URLEncoder.encode("u_id", "UTF-8") + "=" +
                            URLEncoder.encode(u_id, "UTF-8");
                    data += "&" + URLEncoder.encode("subject", "UTF-8") + "=" +
                            URLEncoder.encode(subject, "UTF-8");
                    data += "&" + URLEncoder.encode("pic", "UTF-8") + "=" +
                            URLEncoder.encode("Undefined", "UTF-8");
                    data += "&" + URLEncoder.encode("gen", "UTF-8") + "=" +
                            URLEncoder.encode(String.valueOf(general), "UTF-8");
                    data += "&" + URLEncoder.encode("op1", "UTF-8") + "=" +
                            URLEncoder.encode(opt1, "UTF-8");
                    data += "&" + URLEncoder.encode("op2", "UTF-8") + "=" +
                            URLEncoder.encode(opt2, "UTF-8");
                    data += "&" + URLEncoder.encode("region", "UTF-8") + "=" +
                            URLEncoder.encode(region, "UTF-8");


                    Log.i(TAG, "addSurveyAPI2op: "+u_id+" / "+subject+" / "+String.valueOf(general)+" / "+opt1+" / "+opt2);
                    //Log.i(TAG, "addSurveyAPI2op: URL:"+serverConstants.addSurvey2op_URL);

                    URL url = new URL(serverConstants.addSurvey2op_URL);
                    URLConnection conn = url.openConnection();

                    conn.setDoOutput(true);
                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                    wr.write( data );
                    wr.flush();

                    BufferedReader reader = new BufferedReader(new
                            InputStreamReader(conn.getInputStream()));

                    final StringBuilder sb = new StringBuilder();
                    String line;

                    // Read Server Response
                    while((line = reader.readLine()) != null) {
                        sb.append(line);
                    }

                    System.out.println(sb.toString());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (sb.toString().equals("{\"response\":\"inserted\"}")){
                                Toast.makeText(SurveyAdd.this, "نظرسنجی با موفقیت اضافه شد", Toast.LENGTH_SHORT).show();
                                Log.i(TAG, "addSurveyAPI2op: Inserted");
                            }else if (sb.toString().equals("{\"response\":\"failed\"}")){
                                Toast.makeText(SurveyAdd.this, "اضافه کردن نظرسنجی با خطا مواجه شد", Toast.LENGTH_SHORT).show();
                                Log.i(TAG, "addSurveyAPI2op: Failed");
                            }
                        }
                    });

                } catch (UnsupportedEncodingException e) {
                    Log.i(TAG, "addSurveyAPI2op: error: UnsupportedEncodingException");
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    Log.i(TAG, "addSurveyAPI2op: error: MalformedURLException");
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.i(TAG, "addSurveyAPI2op: error: IOException");
                    e.printStackTrace();
                }

            }
        }).start();
    }
    private void addSurveyAPI4op() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String data = URLEncoder.encode("u_id", "UTF-8") + "=" +
                            URLEncoder.encode(u_id, "UTF-8");
                    data += "&" + URLEncoder.encode("subject", "UTF-8") + "=" +
                            URLEncoder.encode(subject, "UTF-8");
                    data += "&" + URLEncoder.encode("pic", "UTF-8") + "=" +
                            URLEncoder.encode("Undefined", "UTF-8");
                    data += "&" + URLEncoder.encode("gen", "UTF-8") + "=" +
                            URLEncoder.encode(String.valueOf(general), "UTF-8");
                    data += "&" + URLEncoder.encode("op1", "UTF-8") + "=" +
                            URLEncoder.encode(opt1, "UTF-8");
                    data += "&" + URLEncoder.encode("op2", "UTF-8") + "=" +
                            URLEncoder.encode(opt2, "UTF-8");
                    data += "&" + URLEncoder.encode("op3", "UTF-8") + "=" +
                            URLEncoder.encode(opt3, "UTF-8");
                    data += "&" + URLEncoder.encode("op4", "UTF-8") + "=" +
                            URLEncoder.encode(opt4, "UTF-8");
                    data += "&" + URLEncoder.encode("region", "UTF-8") + "=" +
                            URLEncoder.encode(region, "UTF-8");


                    Log.i(TAG, "addSurveyAPI4op: "+u_id+" / "+subject+" / "+String.valueOf(general)+" / "+opt1+" / "+opt2);
                    Log.i(TAG, "addSurveyAPI4op: URL:"+serverConstants.addSurvey4op_URL);

                    URL url = new URL(serverConstants.addSurvey4op_URL);
                    URLConnection conn = url.openConnection();

                    conn.setDoOutput(true);
                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                    wr.write( data );
                    wr.flush();

                    BufferedReader reader = new BufferedReader(new
                            InputStreamReader(conn.getInputStream()));

                    final StringBuilder sb = new StringBuilder();
                    String line;

                    // Read Server Response
                    while((line = reader.readLine()) != null) {
                        sb.append(line);
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (sb.toString().equals("{\"response\":\"inserted\"}")){
                                Toast.makeText(SurveyAdd.this, "نظرسنجی با موفقیت اضافه شد", Toast.LENGTH_SHORT).show();
                                Log.i(TAG, "addSurveyAPI4op: Inserted");
                            }else if (sb.toString().equals("{\"response\":\"failed\"}")){
                                Toast.makeText(SurveyAdd.this, "اضافه کردن نظرسنجی با خطا مواجه شد", Toast.LENGTH_SHORT).show();
                                Log.i(TAG, "addSurveyAPI4op: Failed");
                            }
                        }
                    });

                } catch (UnsupportedEncodingException e) {
                    Log.i(TAG, "addSurveyAPI4op: error: UnsupportedEncodingException");
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    Log.i(TAG, "addSurveyAPI4op: error: MalformedURLException");
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.i(TAG, "addSurveyAPI4op: error: IOException");
                    e.printStackTrace();
                }

            }
        }).start();
    }
    private void addSurveyAPI2opPIC() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String data = URLEncoder.encode("u_id", "UTF-8") + "=" +
                            URLEncoder.encode(u_id, "UTF-8");
                    data += "&" + URLEncoder.encode("subject", "UTF-8") + "=" +
                            URLEncoder.encode(subject, "UTF-8");
                    data += "&" + URLEncoder.encode("pic", "UTF-8") + "=" +
                            URLEncoder.encode(encoded_pic, "UTF-8");
                    data += "&" + URLEncoder.encode("gen", "UTF-8") + "=" +
                            URLEncoder.encode(String.valueOf(general), "UTF-8");
                    data += "&" + URLEncoder.encode("op1", "UTF-8") + "=" +
                            URLEncoder.encode(opt1, "UTF-8");
                    data += "&" + URLEncoder.encode("op2", "UTF-8") + "=" +
                            URLEncoder.encode(opt2, "UTF-8");
                    data += "&" + URLEncoder.encode("region", "UTF-8") + "=" +
                            URLEncoder.encode(region, "UTF-8");


                    Log.i(TAG, "addSurveyAPI2opPIC: "+u_id+" / "+subject+" / "+String.valueOf(general)+" / "+opt1+" / "+opt2);
                    Log.i(TAG, "addSurveyAPI2opPIC: URL:"+serverConstants.addSurvey2opPIC_URL);

                    URL url = new URL(serverConstants.addSurvey2opPIC_URL);
                    URLConnection conn = url.openConnection();

                    conn.setDoOutput(true);
                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                    wr.write( data );
                    wr.flush();

                    BufferedReader reader = new BufferedReader(new
                            InputStreamReader(conn.getInputStream()));

                    final StringBuilder sb = new StringBuilder();
                    String line;

                    // Read Server Response
                    while((line = reader.readLine()) != null) {
                        sb.append(line);
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (sb.toString().equals("{\"response\":\"inserted\"}")){
                                Toast.makeText(SurveyAdd.this, "نظرسنجی با موفقیت اضافه شد", Toast.LENGTH_SHORT).show();
                                Log.i(TAG, "addSurveyAPI2opPIC: Inserted");
                            }else if (sb.toString().equals("{\"response\":\"failed\"}")){
                                Toast.makeText(SurveyAdd.this, "اضافه کردن نظرسنجی با خطا مواجه شد", Toast.LENGTH_SHORT).show();
                                Log.i(TAG, "addSurveyAPI2opPIC: Failed");
                            }
                        }
                    });

                } catch (UnsupportedEncodingException e) {
                    Log.i(TAG, "addSurveyAPI2opPIC: error: UnsupportedEncodingException");
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    Log.i(TAG, "addSurveyAPI2opPIC: error: MalformedURLException");
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.i(TAG, "addSurveyAPI2opPIC: error: IOException");
                    e.printStackTrace();
                }

            }
        }).start();
    }
    private void addSurveyAPI4opPIC() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String data = URLEncoder.encode("u_id", "UTF-8") + "=" +
                            URLEncoder.encode(u_id, "UTF-8");
                    data += "&" + URLEncoder.encode("subject", "UTF-8") + "=" +
                            URLEncoder.encode(subject, "UTF-8");
                    data += "&" + URLEncoder.encode("pic", "UTF-8") + "=" +
                            URLEncoder.encode(encoded_pic, "UTF-8");
                    data += "&" + URLEncoder.encode("gen", "UTF-8") + "=" +
                            URLEncoder.encode(String.valueOf(general), "UTF-8");
                    data += "&" + URLEncoder.encode("op1", "UTF-8") + "=" +
                            URLEncoder.encode(opt1, "UTF-8");
                    data += "&" + URLEncoder.encode("op2", "UTF-8") + "=" +
                            URLEncoder.encode(opt2, "UTF-8");
                    data += "&" + URLEncoder.encode("op3", "UTF-8") + "=" +
                            URLEncoder.encode(opt3, "UTF-8");
                    data += "&" + URLEncoder.encode("op4", "UTF-8") + "=" +
                            URLEncoder.encode(opt4, "UTF-8");
                    data += "&" + URLEncoder.encode("region", "UTF-8") + "=" +
                            URLEncoder.encode(region, "UTF-8");


                    Log.i(TAG, "addSurveyAPI4opPIC: "+u_id+" / "+subject+" / "+String.valueOf(general)+" / "+opt1+" / "+opt2+" / "+opt3+" / "+opt4);
                    Log.i(TAG, "addSurveyAPI4opPIC: URL:"+serverConstants.addSurvey4opPIC_URL);

                    URL url = new URL(serverConstants.addSurvey4opPIC_URL);
                    URLConnection conn = url.openConnection();

                    conn.setDoOutput(true);
                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                    wr.write( data );
                    wr.flush();

                    BufferedReader reader = new BufferedReader(new
                            InputStreamReader(conn.getInputStream()));

                    final StringBuilder sb = new StringBuilder();
                    String line;

                    // Read Server Response
                    while((line = reader.readLine()) != null) {
                        sb.append(line);
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (sb.toString().equals("{\"response\":\"inserted\"}")){
                                Toast.makeText(SurveyAdd.this, "نظرسنجی با موفقیت اضافه شد", Toast.LENGTH_SHORT).show();
                                Log.i(TAG, "addSurveyAPI4opPIC: Inserted");
                            }else if (sb.toString().equals("{\"response\":\"failed\"}")){
                                Toast.makeText(SurveyAdd.this, "اضافه کردن نظرسنجی با خطا مواجه شد", Toast.LENGTH_SHORT).show();
                                Log.i(TAG, "addSurveyAPI4opPIC: Failed");
                            }
                        }
                    });

                } catch (UnsupportedEncodingException e) {
                    Log.i(TAG, "addSurveyAPI4opPIC: error: UnsupportedEncodingException");
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    Log.i(TAG, "addSurveyAPI4opPIC: error: MalformedURLException");
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.i(TAG, "addSurveyAPI4opPIC: error: IOException");
                    e.printStackTrace();
                }

            }
        }).start();
    }
}
