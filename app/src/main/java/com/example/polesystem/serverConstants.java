package com.example.polesystem;

public class serverConstants {
    public static final String ROOT_URL = "http://192.168.1.105:80/survey/";
    public static final String Register_URL = ROOT_URL+"insertUser.php";                      //post
    public static final String SignIn_URL = ROOT_URL+"userAuth.php";                          //get
    public static final String addSurvey2op_URL = ROOT_URL+"insertVote.php";                  //post
    public static final String addSurvey4op_URL = ROOT_URL+"insertVote4Op.php";               //post
    public static final String addSurvey2opPIC_URL = ROOT_URL+"insertVoteWithPic.php";        //post
    public static final String addSurvey4opPIC_URL = ROOT_URL+"insertVoteWithPic4.php";       //post
    public static final String deleteSurvey_URL = ROOT_URL+"deleteVote.php";                  //get
    public static final String getMySurvey_URL = ROOT_URL+"getMySurvey.php";                  //get
    public static final String getSurvey_URL = ROOT_URL+"getSurvey.php";                      //get
    public static final String userInVote_URL = ROOT_URL+"userInVote.php";                    //get
    public static final String getSurveyDetail_URL = ROOT_URL+"getVoteDetail.php";            //get
    public static final String voting_URL = ROOT_URL+"voting.php";                            //get
    public static final String getAllNews_URL = ROOT_URL+"getNews.php";                       //get
    public static final String increaseNewsSeenCount_URL = ROOT_URL+"increaseSeenCount.php";  //post
    public static final String getMySurveyCount_URL = ROOT_URL+"countMySurvey.php";           //get
    public static final String getMyParticipate_URL = ROOT_URL+"getMyParticipate.php";        //get
    public static final String editProfile_URL = ROOT_URL+"editProfile.php";                  //post
    public static final String deleteUser_URL = ROOT_URL+"deleteUser.php";                    //get



}
