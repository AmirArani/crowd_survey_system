package com.example.polesystem;

import android.graphics.Color;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import cz.msebera.android.httpclient.Header;

public class SignUp extends AppCompatActivity {


    private static final String TAG = "npq: SignUp";

    EditText et_name, et_lname, et_personalCode, et_email, et_phone, et_username, et_password;
    TextView tv_areaCode, tv_signStatus;
    Button btn_signup;
    String name = "", lname = "", code = "", email = "", phone = "", username = "", password = "", areaCode = "", street = "", city = "", u_region="";
    GPSTracker gps;
    SQLiteDBHelper dbHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        dbHelper = new SQLiteDBHelper(this);
        dbHelper.insertAreaToDB();

        et_name = findViewById(R.id.editText_signup_name);
        et_lname = findViewById(R.id.editText_signup_lname);
        et_personalCode = findViewById(R.id.editText_signup_personalcode);
        et_email = findViewById(R.id.editText_signup_email);
        et_phone = findViewById(R.id.editText_signup_phone);
        et_username = findViewById(R.id.editText_signup_username);
        et_password = findViewById(R.id.editText_signup_password);
        tv_areaCode = findViewById(R.id.tv_areaCode);
        tv_signStatus = findViewById(R.id.tv_login_status);
        btn_signup = findViewById(R.id.btn_signup);


        gps = new GPSTracker(SignUp.this);
        if (gps.canGetLocation()) {
            String latitude = String.valueOf(gps.getLatitude());
            String longitude = String.valueOf(gps.getLongitude());
            Log.i(TAG, "onCreate: Location: Lat: " + latitude + " Long: " + longitude);
            getDataFromGpsAPI(latitude, longitude);
        } else {
            gps.showSettingsAlert();
        }



        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = et_name.getText().toString().trim();
                lname = et_lname.getText().toString().trim();
                code = et_personalCode.getText().toString().trim();
                email = et_email.getText().toString().trim();
                phone = et_phone.getText().toString().trim();
                username = et_username.getText().toString().trim();
                password = et_password.getText().toString().trim();
                u_region = areaCode;

                if (name.equals("") || lname.equals("") || code.equals("") || email.equals("") ||
                        phone.equals("") || username.equals("") || password.equals("") || u_region.equals("")) {
                    Toast.makeText(SignUp.this, "فیلد های خالی را پر کنید", Toast.LENGTH_SHORT).show();
                } else {
                    // TODO 6.1: _DONE_ insertUser (POST)
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            callAPI();
                        }
                    }).start();
                }
            }
        });
    }


    public void callAPI() {
        try {
            String data = URLEncoder.encode("name", "UTF-8") + "=" +
                    URLEncoder.encode(name, "UTF-8");
            data += "&" + URLEncoder.encode("lname", "UTF-8") + "=" +
                    URLEncoder.encode(lname, "UTF-8");
            data += "&" + URLEncoder.encode("username", "UTF-8") + "=" +
                    URLEncoder.encode(username, "UTF-8");
            data += "&" + URLEncoder.encode("password", "UTF-8") + "=" +
                    URLEncoder.encode(password, "UTF-8");
            data += "&" + URLEncoder.encode("code", "UTF-8") + "=" +
                    URLEncoder.encode(code, "UTF-8");
            data += "&" + URLEncoder.encode("phone", "UTF-8") + "=" +
                    URLEncoder.encode(phone, "UTF-8");
            data += "&" + URLEncoder.encode("email", "UTF-8") + "=" +
                    URLEncoder.encode(email, "UTF-8");
            data += "&" + URLEncoder.encode("region", "UTF-8") + "=" +
                    URLEncoder.encode(u_region, "UTF-8");

            Log.i(TAG, "callAPI: "+name+" / "+lname+" / "+username+" / "+password+" / "+code+"" +
                    " / "+phone+" / "+email+" / "+u_region);
            //Log.i(TAG, "callAPI: URL:"+serverConstants.Register_URL);

            URL url = new URL(serverConstants.Register_URL);
            URLConnection conn = url.openConnection();

            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

            wr.write( data );
            wr.flush();

            BufferedReader reader = new BufferedReader(new
                    InputStreamReader(conn.getInputStream()));

            final StringBuilder sb = new StringBuilder();
            String line;

            // Read Server Response
            while((line = reader.readLine()) != null) {
                sb.append(line);
            }

            System.out.println(sb.toString());
            Log.i(TAG, "callAPI: "+sb.toString());

            tv_signStatus.post(new Runnable() {
                @Override
                public void run() {
                    if (sb.toString().equals("{\"response\":\"user was signed up\"}")){
                        Log.i(TAG, "run: Same User");
                        tv_signStatus.setText("خطا! کاربر با این مشخصات در سیستم موجود است!");
                        tv_signStatus.setTextColor(Color.rgb(255,0,0));
                    }else if (sb.toString().equals("{\"response\":\"user signing up\"}")){
                        Log.i(TAG, "run: OK User");
                        tv_signStatus.setText("کاربر با موفقیت در سیستم ثبت نام شد!");
                        tv_signStatus.setTextColor(Color.rgb(0,255,0));
                    }
                }
            });



        } catch (UnsupportedEncodingException e) {
            Log.i(TAG, "callAPI: error: UnsupportedEncodingException");
            e.printStackTrace();
        } catch (MalformedURLException e) {
            Log.i(TAG, "callAPI: error: MalformedURLException");
            e.printStackTrace();
        } catch (IOException e) {
            Log.i(TAG, "callAPI: error: IOException");
            Toast.makeText(gps, "خطا! اتصال را چک کنید", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

    }

    private void getDataFromGpsAPI(String lat, String lng){
        String url = String.format("https://open.mapquestapi.com/geocoding/v1/reverse?key=Aov09cAOHvzsxmGeKpsLQqCRKPsRBHsZ&location=%s,%s&outFormat=json", lat, lng);
        Log.i(TAG, "getDataFromGpsAPI: "+url);
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(SignUp.this, "Error #1!!!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseString) {
                try {
                    JSONArray object = responseString.getJSONArray("results");
                    JSONObject result = object.getJSONObject(0);
                    JSONArray locations = result.getJSONArray("locations");
                    JSONObject locationsJSONObject = locations.getJSONObject(0);
                    city = locationsJSONObject.getString("adminArea5");
                    street = locationsJSONObject.getString("street");

                    Log.i(TAG, "getDataFromGpsAPI: onSuccess: city: "+city);
                    Log.i(TAG, "getDataFromGpsAPI: onSuccess: street: "+street);

//                    if (city.equals("Kashan")) {
//                        if (street.equals("بلوار علامه قطب رواندی")) {
//                            areaCode = "ناحیه5";
//                        }
//                        else if (street.equals("Amir Kabir")) {
//                            areaCode = "ناحیه1";
//                        }else if (street.equals("شهید رجایی")) {
//                            areaCode = "ناحیه3";
//                        }
//                    } else if (city.equals("طاهرآباد")) {
//                        areaCode = "ناحیه5";
//                    } else if (city.equals("Aran va Bidgol")) {
//                        areaCode = "آران و بیدگل";
//                    } else {
//                        areaCode = "خارج از محدوده مجاز";
//                    }

                    //TODO: get AREA CODE from DB
                    areaCode = dbHelper.getAreafromDB(city, street);

                    Log.i(TAG, "getDataFromGpsAPI: onSuccess: " + areaCode);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tv_areaCode.setText(areaCode);
                        }
                    });
                    Toast.makeText(SignUp.this, "Location Found :)", Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    Toast.makeText(SignUp.this, "Error #2!!!", Toast.LENGTH_LONG).show();
                    Log.i(TAG, "getDataFromGpsAPI: FAILED");
                    e.printStackTrace();
                }
            }
        });
    }
}
